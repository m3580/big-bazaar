package com.bigbazaar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigBazaarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BigBazaarApplication.class, args);
	}

}
